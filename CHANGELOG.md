# Change Log

## [1.2.7] - 2022-10-14
### Changed
-  Added wallet topup attribute as part of Order status response.

## [1.2.6] - 2022-08-18
### Changed
-  Added effectiveAmount,respCode,respMessage attributes as part of Order status response.

## [1.2.5] - 2022-08-18
### Changed
-  Added Offers entity as part of Order status response.

## [1.2.4] - 2022-07-26
### Changed
- Changed metadata type to JsonElement in Wallet class for handling null

## [1.2.3] - 2022-04-25
### Added
- Card entity to support metadata in response. 

## [1.2.2] - 2021-12-20
### Changed
- Log4j dependency version to 2.17.0.

## [1.2.1] - 2021-12-16
### Changed
- Log4j dependency version to 2.16.0.

## [1.2.0] - 2021-12-12
### Changed
- Log4j dependency version to 2.15.0.
- Minimum supported JDK version to 1.8(Log4j 2.15.0 works on JDK 1.8+).

## [1.1.5] - 2021-11-09
### Added
- Token entity.
- Token block in Card entity.

## [1.1.4] - 2021-10-13
### Changed
- Handled Json null while handling non 200 responses in JuspayEntity class.


## [1.1.3] - 2021-07-10
### Added
- Metadata in Refund.
- UPI block in OrderStatus.

## [1.1.2] - 2019-12-10
### Added
- TxnFlowInfo in order status API for VIES parameters.
- getWithClientAuthToken API for Customer entity.
- Provided option to pass parameters in Wallet create and createAndAuthenticate API.
- Metadata support in wallet API response.

## [1.1.1] - 2019-09-04
### Changed
- Added merchantId setup in RequestOptions.

## [1.1.0] - 2019-05-07
### Changed
- Added List Txn API
- Added support for client auth token as part of order and customer APIs
- Added support for Chargeback APIs
- Added support for txnId based refunds and instant refunds
- Merchant Id is included as part of JuspayEnvironment setup and changed PaymentMethod.list() function definition.
- Deprecated list Order API 
- Bugfix in a log line

## [1.0.11] - 2018-11-30
### Changed
- TLS version is set to 1.2 by default for API calls.
- Apache httpclient is replaced with URLConnection. 
- Default API version is changed to 2018-11-30.
- Added TxnDetail, TxnCardInfo, TxnOfferInfo and Mandate entities as part of Order status response.

## [1.0.10] - 2018-05-08
### Added
- CardBalance field in card entity.

## [1.0.9] - 2017-09-14
### Changed
- Fixed bug related to access specifiers in JuspayEntityList class.

## [1.0.8] - 2017-03-22
### Added
- Added AuthorizationException for HTTP status code 403.
### Changed
- Updated internal functionality of card delete. No impact on SDK usage.

## [1.0.7] - 2017-02-20
### Added
- Added paymentMethod and paymentMethodType in Order class.
- Added implementation for Wallet create, createAndAuthenticate, authenticate, link & delink.
- Added implementation for wallet refresh by walletId.
- Added implementation for PaymentMethod list api.

## [1.0.6] - 2016-11-10
### Added
- Added PaymentLinks.java class, which contains the payment links for an order.
- Now the object of Order class will contain a reference to an object of PaymentLinks class.

## [1.0.5] - 2016-10-26
### Changed
- Commented the code to bypass SSL in development environment.

## [1.0.4] - 2016-10-17
### Changed
- Using new Restful API Endpoints in SDK.
- Signature of delete method changed from **delete(Map<String, Object> params)** to **delete(String cardToken)** and from **delete(Map<String, Object> params, RequestOptions requestOptions)** to **delete(String cardToken, RequestOptions requestOptions)** in Card.java.
- Signature of status method changed from **status(Map<String, Object> params)** to **status(String orderId)** and from **status(Map<String, Object> params, RequestOptions requestOptions)** to **status(String orderId, RequestOptions requestOptions)** in Order.java.
- Signature of update method changed from **update(Map<String, Object> params)** to **update(String orderId, Map<String, Object> params)** and from **update(Map<String, Object> params, RequestOptions requestOptions)** to **update(String orderId, Map<String, Object> params, RequestOptions requestOptions)** in Order.java.
- Signature of refund method changed from **refund(Map<String, Object> params)** to **refund(String orderId, Map<String, Object> params)** and from **refund(Map<String, Object> params, RequestOptions requestOptions)** to **refund(String orderId, Map<String, Object> params, RequestOptions requestOptions)** in Order.java.

## [1.0.3] - 2016-10-17
### Added
- Configurable timeouts.
- Improved logging and exception handling.

## [1.0.2] - 2016-08-26
### Removed
- API implementation for Card tokenize. (It's not needed in server side sdk.)

## [1.0.1] - 2016-08-26
### Added
- API implementation for Order create, update, list, status and refund.
- API implementation for Transaction create.
- API implementation for Card create, list, delete and tokenize.
- API implementation for Customer create, update, and get.
- API implementation for Wallet list and refresh.

