package in.juspay.model;

public class PaymentLinks {
    String web;
    String mobile;
    String iframe;

    public String getWebLink() {
        return web;
    }

    public void setWebLink(String web) {
        this.web = web;
    }

    public String getMobileLink() {
        return mobile;
    }

    public void setMobileLink(String mobile) {
        this.mobile = mobile;
    }

    public String getIframeLink() {
        return iframe;
    }

    public void setIframeLink(String iframe) {
        this.iframe = iframe;
    }
}
