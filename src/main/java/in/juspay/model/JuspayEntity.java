package in.juspay.model;

import com.google.gson.*;
import in.juspay.exception.*;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.MapMessage;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import java.io.*;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.Iterator;
import java.util.Map;

public abstract class JuspayEntity {

    private static final Logger log = LogManager.getLogger(JuspayEntity.class);

    private static String serializeParams(Map<String, Object> params) {
        if (params == null || params.size() == 0) {
            return "";
        }
        StringBuilder serializedParams = new StringBuilder();
        try {
            for (String key : params.keySet()) {
                serializedParams.append(key + "=");
                if (params.get(key) != null) {
                    serializedParams.append(URLEncoder.encode(params.get(key).toString(), "UTF-8"));
                }
                serializedParams.append("&");
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Encoding exception while trying to construct payload", e);
        }
        if (serializedParams.charAt(serializedParams.length() - 1) == '&') {
            serializedParams.deleteCharAt(serializedParams.length() - 1);
        }
        return serializedParams.toString();
    }

    protected static JsonObject makeServiceCall(String path, Map<String, Object> params, RequestMethod method, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (requestOptions == null) {
            requestOptions = RequestOptions.createDefault();
        }
        String encodedKey = new String(Base64.encodeBase64(requestOptions.getApiKey().getBytes())).replaceAll("\n", "");
        int httpResponseCode = -1;
        String responseString = null;
        HttpsURLConnection con = null;
        InputStream inputStream = null;
        BufferedReader reader = null;
        try {
            MapMessage mapMessage = new MapMessage();
            if (params != null) {
                for (String key : params.keySet()) {
                    String value = "";
                    if (params.get(key) != null) {
                        value = params.get(key).toString();
                    }
                    mapMessage.put(key, value);
                }
            }
            String endpoint = JuspayEnvironment.getBaseUrl() + path;
            log.info("Executing request: " + method + " " + endpoint);
            log.info("Request parameters: ");
            // Printing this map separately to allow CardNumber and CVV filtering.
            log.info(mapMessage);

            if(method == RequestMethod.GET) {
                String serializedParams = serializeParams(params);
                if(serializedParams != null && !serializedParams.equals("")) {
                    endpoint = endpoint + "?" + serializedParams;
                }
            }

            URL url = new URL(endpoint);
            con = (HttpsURLConnection) url.openConnection();
            String sslProtocol = JuspayEnvironment.getSSLProtocol();
            SSLContext sslContext = SSLContext.getInstance(sslProtocol);
            sslContext.init(null, null, new SecureRandom());
            con.setSSLSocketFactory(sslContext.getSocketFactory());
            con.setConnectTimeout(requestOptions.getConnectTimeoutInMilliSeconds());
            con.setReadTimeout(requestOptions.getReadTimeoutInMilliSeconds());
            con.setRequestProperty("Content-Language", "en-US");
            con.setUseCaches (false);
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("User-Agent", JuspayEnvironment.SDK_VERSION);
            con.setRequestProperty("Authorization", String.format("Basic %s", encodedKey));
            con.setRequestProperty("version", requestOptions.getApiVersion());
            con.setRequestProperty("x-merchantid", requestOptions.getMerchantId());

            if(method == RequestMethod.POST) {
                String payload = serializeParams(params);
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Length", "" + Integer.toString(payload.getBytes().length));
                con.setDoInput(true);
                con.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(payload);
                wr.flush();
                wr.close();
            }
            inputStream = con.getInputStream();
            httpResponseCode = con.getResponseCode();
            reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            String line;
            StringBuilder response = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            responseString = response.toString();
            log.info("Received HTTP Response Code: " + httpResponseCode);
            log.info("Received response: " + responseString);
        } catch (SocketTimeoutException se) {
            log.error("Socket Timeout during request execution: ", se);
            throw new APIConnectionException(-1, "read_timeout", "read_timeout", se.getMessage());
        } catch (IOException ioe) {
            inputStream = con.getErrorStream();
            log.error(String.format("IOException while requesting %s : ", path), ioe);
            try {
                if(inputStream != null) {
                    httpResponseCode = con.getResponseCode();
                    String line;
                    reader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder response = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        response.append(line);
                    }
                    responseString = response.toString();
                } else {
                    log.error("IOException occurred during request execution. Exception is: ", ioe);
                    throw new APIConnectionException(-1, "connection_error", "connection_error", ioe.getMessage());
                }
                log.info("Received HTTP Response Code: " + httpResponseCode);
                log.info("Received response: " + responseString);
            } catch (Exception e) {
                log.error("Exception occured during request execution. Exception is: ", e);
                throw new APIConnectionException(-1, "connection_error", "connection_error", e.getMessage());
            }
        } catch (Exception e) {
            log.error("Exception occurred during request execution. Exception is: ", e);
            throw new APIConnectionException(-1, "connection_error", "connection_error", e.getMessage());
        } finally {
            try {
                if(reader != null) {
                    reader.close();
                }
                if(inputStream != null) {
                    inputStream.close();
                }
                if(con != null) {
                    con.disconnect();
                }
            } catch (Exception e) {
                log.error("Exception occurred while closing the resources. Exception is: ", e);
                throw new APIConnectionException(-1, "connection_error", "connection_error", e.getMessage());
            }
        }
        JsonObject resJson = null;
        try {
            resJson = responseString != null ? new JsonParser().parse(responseString).getAsJsonObject() : null;
        } catch (JsonSyntaxException e) {
            // Do nothing, resJson will remain null.
            log.info("Not able to parse the response into Json. Exception is: ", e);
        }
        if (httpResponseCode >= 200 && httpResponseCode < 300) {
            return resJson;
        } else {
            String status = null;
            String errorCode = null;
            String errorMessage = null;
            if (resJson != null) {
                if (resJson.has("status") && !resJson.get("status").isJsonNull()) {
                    status = resJson.get("status").getAsString();
                }
                if (resJson.has("error_code") && !resJson.get("error_code").isJsonNull()) {
                    errorCode = resJson.get("error_code").getAsString();
                }
                if (resJson.has("error_message") && !resJson.get("error_message").isJsonNull()) {
                    errorMessage = resJson.get("error_message").getAsString();
                }
            }
            switch (httpResponseCode) {
                case 400:
                case 404:
                    throw new InvalidRequestException(httpResponseCode, status, errorCode, errorMessage);
                case 401:
                    throw new AuthenticationException(httpResponseCode, status, errorCode, errorMessage);
                case 403:
                    throw new AuthorizationException(httpResponseCode, status, errorCode, errorMessage);
                default:
                    throw new APIException(httpResponseCode, "internal_error", "internal_error", "Something went wrong.");
            }
        }

    }

    protected static JsonObject addInputParamsToResponse(Map<String, Object> params, JsonObject response) {
        JsonObject inputJson = new JsonParser().parse(new GsonBuilder().create().toJson(params)).getAsJsonObject();
        Iterator<Map.Entry<String, JsonElement>> iterator = inputJson.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, JsonElement> entry = iterator.next();
            response.add(entry.getKey(), entry.getValue());
        }
        return response;
    }

    protected static <T> T createEntityFromResponse(JsonElement response, Class<T> entityClass) {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        T entity = gson.fromJson(response, entityClass);
        return entity;
    }

    protected enum RequestMethod {
        GET, POST, DELETE
    }

}