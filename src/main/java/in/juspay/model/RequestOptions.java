package in.juspay.model;

public class RequestOptions {
    private String apiKey;
    private String merchantId;
    private String apiVersion;
    private int connectTimeoutInMilliSeconds;
    private int readTimeoutInMilliSeconds;

    private RequestOptions() {
        apiKey = JuspayEnvironment.getApiKey();
        merchantId = JuspayEnvironment.getMerchantId();
        connectTimeoutInMilliSeconds = JuspayEnvironment.getConnectTimeoutInMilliSeconds();
        readTimeoutInMilliSeconds = JuspayEnvironment.getReadTimeoutInMilliSeconds();
        apiVersion = JuspayEnvironment.API_VERSION;
    }

    public static RequestOptions createDefault() {
        return new RequestOptions();
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public RequestOptions withMerchantId(String merchantId) {
        this.merchantId = merchantId;
        return this;
    }

    public RequestOptions withApiKey(String apiKey) {
        this.apiKey = apiKey;
        return this;
    }

    public int getConnectTimeoutInMilliSeconds() {
        return connectTimeoutInMilliSeconds;
    }

    public RequestOptions withConnectTimeout(int connectTimeout) {
        this.connectTimeoutInMilliSeconds = connectTimeout;
        return this;
    }

    public int getReadTimeoutInMilliSeconds() {
        return readTimeoutInMilliSeconds;
    }

    public RequestOptions withReadTimeout(int readTimeout) {
        this.readTimeoutInMilliSeconds = readTimeout;
        return this;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public RequestOptions withApiVersion(String version) {
        this.apiVersion = version;
        return this;
    }
}
