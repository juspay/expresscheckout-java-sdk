package in.juspay.model;

import java.util.Date;

public class JuspayOptions {

    private String clientAuthToken;
    private Date clientAuthTokenExpiry;

    public String getClientAuthToken() {
        return clientAuthToken;
    }

    public void setClientAuthToken(String clientAuthToken) {
        this.clientAuthToken = clientAuthToken;
    }

    public Date getClientAuthTokenExpiry() {
        return clientAuthTokenExpiry;
    }

    public void setClientAuthTokenExpiry(Date clientAuthTokenExpiry) {
        this.clientAuthTokenExpiry = clientAuthTokenExpiry;
    }
}
