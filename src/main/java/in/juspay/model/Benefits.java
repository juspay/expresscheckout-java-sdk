package in.juspay.model;

public class Benefits extends JuspayEntity{
    private String type;
    private Double amount;
    private String reference;
    private CalculationInfo calculationInfo;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public CalculationInfo getCalculationInfo() {
        return calculationInfo;
    }

    public void setCalculationInfo(CalculationInfo calculationInfo) {
        this.calculationInfo = calculationInfo;
    }
}