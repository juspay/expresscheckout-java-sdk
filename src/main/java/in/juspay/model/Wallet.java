package in.juspay.model;

import com.google.gson.*;
import com.google.gson.JsonObject;
import in.juspay.exception.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Wallet extends JuspayEntity {

    private String id;
    private String object;
    private String wallet;
    private String token;
    private Boolean linked;
    private Double currentBalance;
    private Date lastRefreshed;
    private String gatewayReferenceId;
    private JsonElement metadata;

    public static WalletList list(String customerId)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return list(customerId, null);
    }

    public static WalletList list(String customerId, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (customerId == null || customerId.equals("")) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/customers/" + customerId + "/wallets", null, RequestMethod.GET, requestOptions);
        return createEntityFromResponse(response, WalletList.class);
    }

    public static WalletList refresh(String customerId)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return refresh(customerId, null);
    }

    public static WalletList refresh(String customerId, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (customerId == null || customerId.equals("")) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/customers/" + customerId + "/wallets/refresh-balances", null, RequestMethod.GET, requestOptions);
        return createEntityFromResponse(response, WalletList.class);
    }

    public static Wallet refreshByWalletId(String walletId)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return refreshByWalletId(walletId, null);
    }

    public static Wallet refreshByWalletId(String walletId, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (walletId == null || walletId.equals("")) {
            throw new InvalidRequestException();
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("command", "refresh");
        JsonObject response = makeServiceCall("/wallets/" + walletId, params, RequestMethod.GET, requestOptions);
        return createEntityFromResponse(response, Wallet.class);
    }

    public static Wallet create(String customerId, String gateway)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return createWallet(customerId, gateway, null, null);
    }

    public static Wallet create(String customerId, String gateway, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return createWallet(customerId, gateway, null, requestOptions);
    }

    public static Wallet create(String customerId, String gateway, Map<String, Object> params, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return createWallet(customerId, gateway, params, requestOptions);
    }

    private static Wallet createWallet(String customerId, String gateway, Map<String, Object> params, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (customerId == null || customerId.equals("") || gateway == null || gateway.equals("")) {
            throw new InvalidRequestException();
        }
        if (params == null) {
            params = new HashMap<String, Object>();
        }
        params.put("gateway", gateway);
        JsonObject response = makeServiceCall("/customers/" + customerId + "/wallets", params, RequestMethod.POST, requestOptions);
        return createEntityFromResponse(response, Wallet.class);
    }

    public static Wallet createAndAuthenticate(String customerId, String gateway)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return createAndAuthenticateWallet(customerId, gateway, null, null);
    }

    public static Wallet createAndAuthenticate(String customerId, String gateway, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return createAndAuthenticateWallet(customerId, gateway, null, requestOptions);
    }

    public static Wallet createAndAuthenticate(String customerId, String gateway, Map<String, Object> params, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return createAndAuthenticateWallet(customerId, gateway, params, requestOptions);
    }

    private static Wallet createAndAuthenticateWallet(String customerId, String gateway, Map<String, Object> params, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (params == null){
            params = new HashMap<String, Object>();
        }
        params.put("command", "authenticate");
        return createWallet(customerId, gateway, params, requestOptions);
    }

    public static Wallet authenticate(String walletId)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return authenticate(walletId, null);
    }

    public static Wallet authenticate(String walletId, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (walletId == null || walletId.equals("")) {
            throw new InvalidRequestException();
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("command", "authenticate");
        JsonObject response = makeServiceCall("/wallets/" + walletId, params, RequestMethod.POST, requestOptions);
        return createEntityFromResponse(response, Wallet.class);
    }

    public static Wallet link(String walletId, String otp)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return link(walletId, otp, null);
    }

    public static Wallet link(String walletId, String otp, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (walletId == null || walletId.equals("") || otp == null || otp.equals("")) {
            throw new InvalidRequestException();
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("otp",otp);
        params.put("command", "link");
        JsonObject response = makeServiceCall("/wallets/" + walletId, params, RequestMethod.POST, requestOptions);
        return createEntityFromResponse(response, Wallet.class);
    }

    public static Wallet delink(String walletId)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return delink(walletId, null);
    }

    public static Wallet delink(String walletId, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (walletId == null || walletId.equals("")) {
            throw new InvalidRequestException();
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("command", "delink");
        JsonObject response = makeServiceCall("/wallets/" + walletId, params, RequestMethod.POST, requestOptions);
        return createEntityFromResponse(response, Wallet.class);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Double getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(Double currentBalance) {
        this.currentBalance = currentBalance;
    }

    public Date getLastRefreshed() {
        return lastRefreshed;
    }

    public void setLastRefreshed(Date lastRefreshed) {
        this.lastRefreshed = lastRefreshed;
    }

    public Boolean getLinked() { return linked; }

    public void setLinked(Boolean linked) { this.linked = linked; }

    public String getGatewayReferenceId() { return gatewayReferenceId; }

    public void setGatewayReferenceId(String gatewayReferenceId) { this.gatewayReferenceId = gatewayReferenceId; }

    public JsonElement getMetadata() { return metadata; }

    public void setMetadata(JsonElement metadata) { this.metadata = metadata; }
}
