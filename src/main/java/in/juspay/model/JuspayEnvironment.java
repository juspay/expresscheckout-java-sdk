package in.juspay.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class JuspayEnvironment {
    public static final String DEVELOPMENT_BASE_URL = "https://localapi.juspay.in";
    public static final String SANDBOX_BASE_URL = "https://sandbox.juspay.in";
    public static final String PRODUCTION_BASE_URL = "https://api.juspay.in";
    public static final String API_VERSION = "2019-05-07";
    public static final String DEFAULT_SSL_PROTOCOL = "TLSv1.2";
    private static final Logger log = LogManager.getLogger(JuspayEnvironment.class);
    public static final String SDK_VERSION = getSdkVersion();
    private static volatile String baseUrl = JuspayEnvironment.PRODUCTION_BASE_URL;
    private static volatile String apiKey;
    private static volatile String merchantId;
    private static volatile int connectTimeoutInMilliSeconds;
    private static volatile int readTimeoutInMilliSeconds;
    private static volatile String sslProtocol = DEFAULT_SSL_PROTOCOL;

    public static JuspayEnvironment withBaseUrl(String newBaseUrl) {
        baseUrl = newBaseUrl;
        return null;
    }

    public static JuspayEnvironment withApiKey(String newApiKey) {
        apiKey = newApiKey;
        return null;
    }

    public static JuspayEnvironment withMerchantId(String newMerchantId) {
        merchantId = newMerchantId;
        return null;
    }

    public static JuspayEnvironment withSSLProtocol(String protocol) {
        sslProtocol = protocol;
        return null;
    }

    public static JuspayEnvironment withConnectTimeoutInMilliSeconds(int connectTimeoutInMilliSeconds) {
        JuspayEnvironment.connectTimeoutInMilliSeconds = connectTimeoutInMilliSeconds;
        return null;
    }

    public static JuspayEnvironment withReadTimeoutInMilliSeconds(int readTimeoutInMilliSeconds) {
        JuspayEnvironment.readTimeoutInMilliSeconds = readTimeoutInMilliSeconds;
        return null;
    }

    public static String getBaseUrl() {
        return baseUrl;
    }

    public static String getApiKey() {
        return apiKey;
    }

    public static String getMerchantId() {
        return merchantId;
    }

    public static String getSSLProtocol() {
        return sslProtocol;
    }

    public static int getConnectTimeoutInMilliSeconds() {
        return connectTimeoutInMilliSeconds;
    }

    public static int getReadTimeoutInMilliSeconds() {
        return readTimeoutInMilliSeconds;
    }

    private static String getSdkVersion() {
        Properties prop = new Properties();
        InputStream inputStream = JuspayEntity.class.getClassLoader().getResourceAsStream("config.properties");
        try {
            prop.load(inputStream);
            return "JAVA_SDK/" + prop.getProperty("sdk.version");
        } catch (IOException e) {
            log.info("Exception in reading properties file: " + e.getMessage());
            return "";
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                // Do nothing.
            }
        }
    }

}
