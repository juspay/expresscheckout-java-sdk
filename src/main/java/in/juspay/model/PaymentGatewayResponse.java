package in.juspay.model;

import java.util.Date;
import java.util.Map;

public class PaymentGatewayResponse extends JuspayEntity {
    private String rrn;
    private String epgTxnId;
    private String authIdCode;
    private String txnId;
    private String respCode;
    private String respMessage;
    private Date created;
    private String offer;
    private String offerType;
    private String offerAvailed;
    private Double discountAmount;
    private Map<Object, Object> gatewayResponse;

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getEpgTxnId() {
        return epgTxnId;
    }

    public void setEpgTxnId(String epgTxnId) {
        this.epgTxnId = epgTxnId;
    }

    public String getAuthIdCode() {
        return authIdCode;
    }

    public void setAuthIdCode(String authIdCode) {
        this.authIdCode = authIdCode;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getOfferAvailed() {
        return offerAvailed;
    }

    public void setOfferAvailed(String offerAvailed) {
        this.offerAvailed = offerAvailed;
    }

    public Double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Map<Object, Object> getGatewayResponse() {
        return gatewayResponse;
    }

    public void setGatewayResponse(Map<Object, Object> gatewayResponse) {
        this.gatewayResponse = gatewayResponse;
    }
}
