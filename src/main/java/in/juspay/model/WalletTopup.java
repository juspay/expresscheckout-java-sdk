package in.juspay.model;

public class WalletTopup extends JuspayEntity {
    String txnFlowType ;
    WalletTopupStatus topup;

    public String getTxnFlowType() { return txnFlowType; }

    public void setTxnFlowType(String txnFlowType) { this.txnFlowType = txnFlowType; }
    
    public WalletTopupStatus getTopup() { return topup; }

    public void setTopup(WalletTopupStatus topup) { this.topup = topup; }

}
