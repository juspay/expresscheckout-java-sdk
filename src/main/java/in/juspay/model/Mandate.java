package in.juspay.model;

public class Mandate extends JuspayEntity {
    private String mandateId;
    private String mandateToken;
    private String mandateStatus;

    public String getMandateId() {
        return mandateId;
    }

    public void setMandateId(String mandateId) {
        this.mandateId = mandateId;
    }

    public String getMandateToken() {
        return mandateToken;
    }

    public void setMandateToken(String mandateToken) {
        this.mandateToken = mandateToken;
    }

    public String getMandateStatus() {
        return mandateStatus;
    }

    public void setMandateStatus(String mandateStatus) {
        this.mandateStatus = mandateStatus;
    }
}
