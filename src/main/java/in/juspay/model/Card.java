package in.juspay.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import in.juspay.exception.*;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Card extends JuspayEntity {

    private String cardNumber;
    private String nameOnCard;
    private String cardExpYear;
    private String cardExpMonth;
    private String expiryMonth;
    private String expiryYear;
    private String cardSecurityCode;
    private String nickname;
    private String cardToken;
    private String cardReference;
    private String cardFingerprint;
    private String cardIsin;
    private String lastFourDigits;
    private String cardType;
    private String cardIssuer;
    private Boolean savedToLocker;
    private Boolean expired;
    private String cardBrand;
    private Double cardBalance;
    private Boolean usingSavedCard;
    private String  cardSubType;
    private String  cardIssuerCountry;
    private String  juspayBankCode;
    private Boolean usingToken;
    private Boolean tokenizationUserConsent;
    private Boolean tokenizeSupport;
    private String  providerCategory;
    private String  provider;
    private Token   token;
    private Map<String, Object> metadata;

    public static Card create(Map<String, Object> params)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return create(params, null);
    }

    public static Card create(Map<String, Object> params, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (params == null || params.size() == 0) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/cards", params, RequestMethod.POST, requestOptions);
        response = addInputParamsToResponse(params, response);
        return createEntityFromResponse(response, Card.class);
    }

    public static List<Card> list(Map<String, Object> params)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return list(params, null);
    }

    public static List<Card> list(Map<String, Object> params, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (params == null || params.size() == 0) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/cards", params, RequestMethod.GET, requestOptions);
        List<Card> cardList = new ArrayList<Card>();
        if (response.has("cards")) {
            JsonArray cardArray = response.get("cards").getAsJsonArray();
            for (int i = 0; i < cardArray.size(); i++) {
                cardList.add(createEntityFromResponse(cardArray.get(i), Card.class));
            }
        }
        return cardList;
    }

    public static boolean delete(String cardToken)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return delete(cardToken, null);
    }

    public static boolean delete(String cardToken, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (cardToken == null || cardToken.length() == 0) {
            throw new InvalidRequestException();
        }
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("card_token", cardToken);
        JsonObject response = makeServiceCall("/card/delete", params, RequestMethod.POST, requestOptions);
        return response.get("deleted").getAsBoolean();
    }

    public Boolean getSavedToLocker() {
        return savedToLocker;
    }

    public void setSavedToLocker(Boolean savedToLocker) {
        this.savedToLocker = savedToLocker;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getNameOnCard() {
        return nameOnCard;
    }

    public void setNameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }

    public String getCardExpYear() {
        if(expiryYear != null) {
            return expiryYear;
        }
        return cardExpYear;
    }

    public void setCardExpYear(String cardExpYear) {
        this.cardExpYear = cardExpYear;
    }

    public String getCardExpMonth() {
        if(expiryMonth != null) {
            return expiryMonth;
        }
        return cardExpMonth;
    }

    public void setCardExpMonth(String cardExpMonth) {
        this.cardExpMonth = cardExpMonth;
    }

    public String getCardSecurityCode() {
        return cardSecurityCode;
    }

    public void setCardSecurityCode(String cardSecurityCode) {
        this.cardSecurityCode = cardSecurityCode;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public String getCardReference() {
        return cardReference;
    }

    public void setCardReference(String cardReference) {
        this.cardReference = cardReference;
    }

    public String getCardFingerprint() {
        return cardFingerprint;
    }

    public void setCardFingerprint(String cardFingerprint) {
        this.cardFingerprint = cardFingerprint;
    }

    public String getCardIsin() {
        return cardIsin;
    }

    public void setCardIsin(String cardIsin) {
        this.cardIsin = cardIsin;
    }

    public String getLastFourDigits() {
        return lastFourDigits;
    }

    public void setLastFourDigits(String lastFourDigits) {
        this.lastFourDigits = lastFourDigits;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardIssuer() {
        return cardIssuer;
    }

    public void setCardIssuer(String cardIssuer) {
        this.cardIssuer = cardIssuer;
    }

    public Boolean getExpired() {
        return expired;
    }

    public void setExpired(Boolean expired) {
        this.expired = expired;
    }

    public String getCardBrand() {
        return cardBrand;
    }

    public void setCardBrand(String cardBrand) {
        this.cardBrand = cardBrand;
    }

    public Double getCardBalance() { return cardBalance; }

    public void setCardBalance(Double cardBalance) {this.cardBalance = cardBalance;}

    public Boolean getUsingSavedCard() {
        return usingSavedCard;
    }

    public void setUsingSavedCard(Boolean usingSavedCard) {
        this.usingSavedCard = usingSavedCard;
    }

    public String getExpiryMonth() {
        if(cardExpMonth != null) {
            return cardExpMonth;
        }
        return expiryMonth;
    }

    public void setExpiryMonth(String expiryMonth) {
        this.expiryMonth = expiryMonth;
    }

    public String getExpiryYear() {
        if(cardExpYear != null) {
            return cardExpYear;
        }
        return expiryYear;
    }

    public void setExpiryYear(String expiryYear) {
        this.expiryYear = expiryYear;
    }

    public String getCardSubType() {
        return cardSubType;
    }

    public void setCardSubType(String cardSubType) {
        this.cardSubType = cardSubType;
    }

    public Boolean getTokenizationUserConsent() {
        return tokenizationUserConsent;
    }

    public void setTokenizationUserConsent(Boolean cardSubType) { this.tokenizationUserConsent = tokenizationUserConsent; }

    public Boolean getUsingToken() {
        return usingToken;
    }

    public void setUsingToken(Boolean usingToken) {
        this.usingToken = usingToken;
    }

    public Token getToken(){
        return token;
    }

    public void setToken(Token token){
        this.token = token;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProviderCategory() { return providerCategory; }

    public void setProviderCategory(String providerCategory) {
        this.providerCategory = providerCategory;
    }

    public Boolean getTokenizeSupport() {
        return tokenizeSupport;
    }

    public void setTokenizeSupport(Boolean tokenizeSupport) {
        this.tokenizeSupport = tokenizeSupport;
    }
   
    public String getCardIssuerCountry() {
        return cardIssuerCountry;
    }

    public void setCardIssuerCountry(String cardIssuerCountry) {
        this.cardIssuerCountry = cardIssuerCountry;
    }

    public String getJuspayBankCode() {
        return juspayBankCode;
    }

    public void setJuspayBankCode(String juspayBankCode) {
        this.juspayBankCode = juspayBankCode;
    }

    public Map<String, Object> getMetadata() { return metadata; }

    public void setMetadata(Map<String, Object> metadata) { this.metadata = metadata; }
}
