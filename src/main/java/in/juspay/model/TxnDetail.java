package in.juspay.model;

import in.juspay.exception.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class TxnDetail extends JuspayEntity {
    private String txnId;
    private String orderId;
    private String txnUuid;
    private String gatewayId;
    private String status;
    private String gateway;
    private Boolean expressCheckout;
    private Boolean redirect;
    private Double netAmount;
    private Double surchargeAmount;
    private Double taxAmount;
    private Double txnAmount;
    private String currency;
    private String errorMessage;
    private String errorCode;
    private Date created;
    private String txnObjectType;
    private String sourceObject;
    private String sourceObjectId;
    private Boolean isConflicted;
    private Boolean isEmi;
    private Integer emiTenure;
    private String emiBank;
    private Double refundedAmount;
    private Boolean refundedEntirely;
    private TxnCardInfo txnCardInfo;
    private PaymentInfo paymentInfo;
    private PaymentGatewayResponse paymentGatewayResponse;
    private List<Refund> refunds;
    private TxnRiskInfo risk;
    private List<Chargeback> chargebacks;

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTxnUuid() {
        return txnUuid;
    }

    public void setTxnUuid(String txnUuid) {
        this.txnUuid = txnUuid;
    }

    public String getGatewayId() {
        return gatewayId;
    }

    public void setGatewayId(String gatewayId) {
        this.gatewayId = gatewayId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public Boolean getExpressCheckout() {
        return expressCheckout;
    }

    public void setExpressCheckout(Boolean expressCheckout) {
        this.expressCheckout = expressCheckout;
    }

    public Boolean getRedirect() {
        return redirect;
    }

    public void setRedirect(Boolean redirect) {
        this.redirect = redirect;
    }

    public Double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(Double netAmount) {
        this.netAmount = netAmount;
    }

    public Double getSurchargeAmount() {
        return surchargeAmount;
    }

    public void setSurchargeAmount(Double surchargeAmount) {
        this.surchargeAmount = surchargeAmount;
    }

    public Double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(Double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public Double getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(Double txnAmount) {
        this.txnAmount = txnAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getTxnObjectType() {
        return txnObjectType;
    }

    public void setTxnObjectType(String txnObjectType) {
        this.txnObjectType = txnObjectType;
    }

    public String getSourceObject() {
        return sourceObject;
    }

    public void setSourceObject(String sourceObject) {
        this.sourceObject = sourceObject;
    }

    public String getSourceObjectId() {
        return sourceObjectId;
    }

    public void setSourceObjectId(String sourceObjectId) {
        this.sourceObjectId = sourceObjectId;
    }

    public Boolean getConflicted() {
        return isConflicted;
    }

    public void setConflicted(Boolean conflicted) {
        isConflicted = conflicted;
    }

    public Double getRefundedAmount() {
        return refundedAmount;
    }

    public void setRefundedAmount(Double refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    public Boolean getRefundedEntirely() {
        return refundedEntirely;
    }

    public void setRefundedEntirely(Boolean refundedEntirely) {
        this.refundedEntirely = refundedEntirely;
    }

    public TxnCardInfo getTxnCardInfo() {
        return txnCardInfo;
    }

    public void setTxnCardInfo(TxnCardInfo txnCardInfo) {
        this.txnCardInfo = txnCardInfo;
    }

    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }


    public PaymentGatewayResponse getPaymentGatewayResponse() {
        return paymentGatewayResponse;
    }

    public void setPaymentGatewayResponse(PaymentGatewayResponse paymentGatewayResponse) {
        this.paymentGatewayResponse = paymentGatewayResponse;
    }

    public List<Refund> getRefunds() {
        return refunds;
    }

    public void setRefunds(List<Refund> refunds) {
        this.refunds = refunds;
    }

    public TxnRiskInfo getRiskInfo() {
        return risk;
    }

    public void setRiskInfo(TxnRiskInfo riskInfo) {
        this.risk = riskInfo;
    }

    public List<Chargeback> getChargebacks() {
        return chargebacks;
    }

    public void setChargebacks(List<Chargeback> chargebacks) {
        this.chargebacks = chargebacks;
    }


    public Boolean getEmi() {
        return isEmi;
    }

    public void setEmi(Boolean emi) {
        isEmi = emi;
    }

    public Integer getEmiTenure() {
        return emiTenure;
    }

    public void setEmiTenure(Integer emiTenure) {
        this.emiTenure = emiTenure;
    }

    public String getEmiBank() {
        return emiBank;
    }

    public void setEmiBank(String emiBank) {
        this.emiBank = emiBank;
    }

    public static Refund refund(Map<String, Object> params)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return refund(params, null);
    }

    public static Refund refund(Map<String, Object> params, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return Order.refund(params, requestOptions);
    }
}
