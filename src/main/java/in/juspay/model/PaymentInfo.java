package in.juspay.model;

public class PaymentInfo {
    private String paymentMethod;
    private String paymentMethodType;
    private String emiBank;
    private Integer emiTenure;
    private Card card;
    private String authType;
    private String payerVpa;
    private String payerAppName;
    private Authentication authentication;

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public String getEmiBank() {
        return emiBank;
    }

    public void setEmiBank(String emiBank) {
        this.emiBank = emiBank;
    }

    public Integer getEmiTenure() {
        return emiTenure;
    }

    public void setEmiTenure(Integer emiTenure) {
        this.emiTenure = emiTenure;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getPayerVpa() {
        return payerVpa;
    }

    public void setPayerVpa(String payerVpa) {
        this.payerVpa = payerVpa;
    }

    public String getPayerAppName() {
        return payerAppName;
    }

    public void setPayerAppName(String payerAppName) {
        this.payerAppName = payerAppName;
    }

    public Authentication getAuthentication() {
        return authentication;
    }

    public void setAuthentication(Authentication authentication) {
        this.authentication = authentication;
    }
}
