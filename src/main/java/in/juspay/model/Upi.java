package in.juspay.model;

public class Upi {
    private String txnFlowType;
    private String maskedBankAccountNumber;
    private String bankName;
    private String bankCode;
    private String upiBankAccountReferenceId;
    private String payerVpa;
    private String payerApp;
    private String juspayBankCode;

    public String getTxnFlowType() {
        return txnFlowType;
    }

    public void setTxnFlowType(String txnFlowType) {
        this.txnFlowType = txnFlowType;
    }

    public String getMaskedBankAccountNumber() {
        return maskedBankAccountNumber;
    }

    public void setMaskedBankAccountNumber(String maskedBankAccountNumber) {
        this.maskedBankAccountNumber = maskedBankAccountNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getUpiBankAccountReferenceId() {
        return upiBankAccountReferenceId;
    }

    public void setUpiBankAccountReferenceId(String upiBankAccountReferenceId) {
        this.upiBankAccountReferenceId = upiBankAccountReferenceId;
    }

    public String getPayerVpa() {
        return payerVpa;
    }

    public void setPayerVpa(String payerVpa) {
        this.payerVpa = payerVpa;
    }

    public String getPayerApp() {
        return payerApp;
    }

    public void setPayerApp(String payerApp) {
        this.payerApp = payerApp;
    }

    public String getJuspayBankCode() {
        return juspayBankCode;
    }

    public void setJuspayBankCode(String juspayBankCode) {
        this.juspayBankCode = juspayBankCode;
    }
}
