package in.juspay.model;

import com.google.gson.JsonObject;

import java.util.Date;

public class Refund extends JuspayEntity {
    private String id;
    private String uniqueRequestId;
    private String ref;
    private Double amount;
    private Date created;
    private String status;
    private String errorMessage;
    private String initiatedBy;
    private Boolean sentToGateway;
    private String arn;
    private String internalReferenceId;
    private String gateway;
    private String epgTxnId;
    private String authorizationId;
    private String referenceId;
    private String responseCode;
    private String refundArn;
    private String refundType;
    private String refundSource;
    private String txnId;
    private String orderId;
    private JsonObject metadata;

    public String getId() {
        if(referenceId != null) {
            return referenceId;
        }
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUniqueRequestId() {
        return uniqueRequestId;
    }

    public void setUniqueRequestId(String uniqueRequestId) {
        this.uniqueRequestId = uniqueRequestId;
    }

    public String getRef() {
        if(epgTxnId != null) {
            return epgTxnId;
        }
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public Boolean getSentToGateway() {
        return sentToGateway;
    }

    public void setSentToGateway(Boolean sentToGateway) {
        this.sentToGateway = sentToGateway;
    }

    public String getArn() {
        if(refundArn != null) {
            return refundArn;
        }
        return arn;
    }

    public void setArn(String arn) {
        this.arn = arn;
    }

    public String getInternalReferenceId() {
        return internalReferenceId;
    }

    public void setInternalReferenceId(String internalReferenceId) {
        this.internalReferenceId = internalReferenceId;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public String getEpgTxnId() {
        if(ref != null) {
            return ref;
        }
        return epgTxnId;
    }

    public void setEpgTxnId(String epgTxnId) {
        this.epgTxnId = epgTxnId;
    }

    public String getAuthorizationId() {
        return authorizationId;
    }

    public void setAuthorizationId(String authorizationId) {
        this.authorizationId = authorizationId;
    }

    public String getReferenceId() {
        if(id != null) {
            return id;
        }
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getRefundArn() {
        if(arn != null) {
            return arn;
        }
        return refundArn;
    }

    public void setRefundArn(String refundArn) {
        this.refundArn = refundArn;
    }

    public String getRefundType() {
        return refundType;
    }

    public void setRefundType(String refundType) {
        this.refundType = refundType;
    }

    public String getRefundSource() {
        return refundSource;
    }

    public void setRefundSource(String refundSource) {
        this.refundSource = refundSource;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public JsonObject getMetadata() { return metadata; }

    public void setMetadata(JsonObject metadata) { this.metadata = metadata; }
}
