package in.juspay.model;

import java.util.List;

public abstract class JuspayEntityList<T extends JuspayEntity> {
    private List<T> list;
    private long count;
    private long offset;
    private long total;

    public List<T> getList() {
        return list;
    }

    public long getCount() {
        return count;
    }

    public long getOffset() {
        return offset;
    }

    public long getTotal() {
        return total;
    }
}
