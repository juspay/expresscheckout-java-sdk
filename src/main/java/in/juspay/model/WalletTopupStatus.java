package in.juspay.model;

public class WalletTopupStatus extends JuspayEntity {
    String walletId;
    String status;
    String responseMessage;
    String topupTxnId;
    Double amount;
    String epgTxnId;
    
    public String getWalletId() { return walletId; }

    public void setWalletId(String walletId) { this.walletId = walletId; }
 
    public String getStatus() { return status; }

    public void setStatus(String status) { this.status = status; }

    public String getResponseMessage() { return responseMessage; }

    public void setResponseMessage(String responseMessage) { this.responseMessage = responseMessage; }

    public String getTopupTxnId() { return topupTxnId; }

    public void setTopupTxnId(String topupTxnId) { this.topupTxnId = topupTxnId; }

    public Double getAmount() { return amount; }

    public void setAmount(Double amount) { this.amount = amount; }

    public String getEpgTxnId() { return epgTxnId; }

    public void setEpgTxnId(String epgTxnId) { this.epgTxnId = epgTxnId; }

}