package in.juspay.model;

import com.google.gson.JsonObject;
import in.juspay.exception.*;

import java.util.Date;
import java.util.Map;

public class Chargeback extends JuspayEntity {
    private String id;
    private String object;
    private String objectReferenceId;
    private Date dateResolved;
    private String disputeStatus;
    private Double amount;
    private TxnDetail txn;
    private Date dateCreated;
    private Date lastUpdated;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getObjectReferenceId() {
        return objectReferenceId;
    }

    public void setObjectReferenceId(String objectReferenceId) {
        this.objectReferenceId = objectReferenceId;
    }

    public Date getDateResolved() {
        return dateResolved;
    }

    public void setDateResolved(Date dateResolved) {
        this.dateResolved = dateResolved;
    }

    public String getDisputeStatus() {
        return disputeStatus;
    }

    public void setDisputeStatus(String disputeStatus) {
        this.disputeStatus = disputeStatus;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public TxnDetail getTxn() {
        return txn;
    }

    public void setTxn(TxnDetail txn) {
        this.txn = txn;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public static Chargeback create(Map<String, Object> params)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return create(params, null);
    }

    public static Chargeback create(Map<String, Object> params, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (params == null || params.size() == 0) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/chargebacks", params, RequestMethod.POST, requestOptions);
        return createEntityFromResponse(response, Chargeback.class);
    }

    public static Chargeback update(String id, Map<String, Object> params)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return update(id, params, null);
    }

    public static Chargeback update(String id, Map<String, Object> params, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (id == null || id.equals("") || params == null || params.size() == 0) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/chargebacks/" + id, params, RequestMethod.POST, requestOptions);
        return createEntityFromResponse(response, Chargeback.class);
    }

    public static Chargeback get(String id)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return get(id, null);
    }

    public static Chargeback get(String id, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (id == null || id.equals("")) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/chargebacks/" + id, null, RequestMethod.GET, requestOptions);
        return createEntityFromResponse(response, Chargeback.class);
    }
}
