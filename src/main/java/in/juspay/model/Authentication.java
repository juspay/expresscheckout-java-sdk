package in.juspay.model;

public class Authentication {
    private SecondFactorResponse secondFactorResponse;

    public SecondFactorResponse getSecondFactorResponse() {
        return secondFactorResponse;
    }

    public void setSecondFactorResponse(SecondFactorResponse secondFactorResponse) {
        this.secondFactorResponse = secondFactorResponse;
    }
}
