package in.juspay.model;

public class Token extends JuspayEntity {
    private String cardReference;
    private String lastFourDigits;
    private String cardFingerprint;
    private String cardIsin;
    private String expiryYear;
    private String expiryMonth;
    private String tokenizationStatus;
    private String par;

    public String getTokenizationStatus() {
        return tokenizationStatus;
    }

    public void setTokenizationStatus(String tokenizationStatus) {
        this.tokenizationStatus = tokenizationStatus;
    }

    public String getPar() {
        return par;
    }

    public void setPar(String par) {
        this.par = par;
    }

    public String getExpiryMonth() {
        return expiryMonth;
    }

    public void setExpiryMonth(String expiryMonth) {
        this.expiryMonth = expiryMonth;
    }

    public String getExpiryYear() {
        return expiryYear;
    }

    public void setExpiryYear(String expiryYear) {
        this.expiryYear = expiryYear;
    }

    public String getCardReference() {
        return cardReference;
    }

    public void setCardReference(String cardReference) {
        this.cardReference = cardReference;
    }

    public String getCardFingerprint() {
        return cardFingerprint;
    }

    public void setCardFingerprint(String cardFingerprint) {
        this.cardFingerprint = cardFingerprint;
    }

    public String getCardIsin() {
        return cardIsin;
    }

    public void setCardIsin(String cardIsin) {
        this.cardIsin = cardIsin;
    }

    public String getLastFourDigits() {
        return lastFourDigits;
    }

    public void setLastFourDigits(String lastFourDigits) {
        this.lastFourDigits = lastFourDigits;
    }

}
