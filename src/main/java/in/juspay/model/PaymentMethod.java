package in.juspay.model;

import com.google.gson.JsonObject;
import in.juspay.exception.*;

public class PaymentMethod extends JuspayEntity {

    String paymentMethodType;
    String paymentMethod;
    String description;

    public static PaymentMethodList list()
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return list(null);
    }

    public static PaymentMethodList list(RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        String merchantId = JuspayEnvironment.getMerchantId();
        if (merchantId == null || merchantId.equals("")) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/merchants/" + merchantId + "/paymentmethods", null, RequestMethod.GET, requestOptions);
        return createEntityFromResponse(response, PaymentMethodList.class);
    }

    public String getPaymentMethodType() {return paymentMethodType;}

    public void setPaymentMethodType(String paymentMethodType) {this.paymentMethodType = paymentMethodType;}

    public String getPaymentMethod() {return paymentMethod;}

    public void setPaymentMethod(String paymentMethod) {this.paymentMethod = paymentMethod;}

    public String getDescription() {return description;}

    public void setDescription(String description) {this.description = description;}
}
