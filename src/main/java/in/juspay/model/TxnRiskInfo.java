package in.juspay.model;

public class TxnRiskInfo {
    private String provider;
    private String status;
    private String message;
    private Boolean flagged;
    private String recommendedAction;
    private String ebsRiskLevel;
    private String ebsPaymentStatus;
    private Integer ebsRiskPercentage;
    private String ebsBinCountry;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getFlagged() {
        return flagged;
    }

    public void setFlagged(Boolean flagged) {
        this.flagged = flagged;
    }

    public String getRecommendedAction() {
        return recommendedAction;
    }

    public void setRecommendedAction(String recommendedAction) {
        this.recommendedAction = recommendedAction;
    }

    public String getEbsRiskLevel() {
        return ebsRiskLevel;
    }

    public void setEbsRiskLevel(String ebsRiskLevel) {
        this.ebsRiskLevel = ebsRiskLevel;
    }

    public String getEbsPaymentStatus() {
        return ebsPaymentStatus;
    }

    public void setEbsPaymentStatus(String ebsPaymentStatus) {
        this.ebsPaymentStatus = ebsPaymentStatus;
    }

    public Integer getEbsRiskPercentage() {
        return ebsRiskPercentage;
    }

    public void setEbsRiskPercentage(Integer ebsRiskPercentage) {
        this.ebsRiskPercentage = ebsRiskPercentage;
    }

    public String getEbsBinCountry() {
        return ebsBinCountry;
    }

    public void setEbsBinCountry(String ebsBinCountry) {
        this.ebsBinCountry = ebsBinCountry;
    }
}
