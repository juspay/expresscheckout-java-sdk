package in.juspay.model;

import java.util.Date;

public class SecondFactorResponse {
    private String cavv;
    private String eci;
    private String xid;
    private String status;
    private String currency;
    private String responseId;
    private String mpiErrorCode;
    private Date dateCreated;
    private String paresStatus;

    public String getCavv() {
        return cavv;
    }

    public void setCavv(String cavv) {
        this.cavv = cavv;
    }

    public String getEci() {
        return eci;
    }

    public void setEci(String eci) {
        this.eci = eci;
    }

    public String getXid() {
        return xid;
    }

    public void setXid(String xid) {
        this.xid = xid;
    }

    public String getStatus() {
        if (paresStatus != null) {
            return paresStatus;
        } else {
            return status;
        }
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    public String getMpiErrorCode() {
        return mpiErrorCode;
    }

    public void setMpiErrorCode(String mpiErrorCode) {
        this.mpiErrorCode = mpiErrorCode;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
