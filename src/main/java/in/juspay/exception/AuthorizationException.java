package in.juspay.exception;

public class AuthorizationException extends JuspayException {
    public AuthorizationException(int httpResponseCode, String status, String errorCode, String errorMessage) {
        super(httpResponseCode, status, errorCode, errorMessage);
    }
}