package in.juspay.exception;

public class APIConnectionException extends JuspayException {
    public APIConnectionException(int httpResponseCode, String status, String errorCode, String errorMessage) {
        super(httpResponseCode, status, errorCode, errorMessage);
    }
}
