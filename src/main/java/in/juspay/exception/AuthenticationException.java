package in.juspay.exception;

public class AuthenticationException extends JuspayException {
    public AuthenticationException(int httpResponseCode, String status, String errorCode, String errorMessage) {
        super(httpResponseCode, status, errorCode, errorMessage);
    }
}
