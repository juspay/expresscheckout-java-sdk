package in.juspay.exception;

public class APIException extends JuspayException {
    public APIException(int httpResponseCode, String status, String errorCode, String errorMessage) {
        super(httpResponseCode, status, errorCode, errorMessage);
    }
}
