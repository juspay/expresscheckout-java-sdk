package in.juspay.test;

import in.juspay.exception.JuspayException;
import in.juspay.model.RequestOptions;
import in.juspay.model.Wallet;
import in.juspay.model.WalletList;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class WalletTest {

    CustomerTest customerTest;
    Wallet wallet;

    @Before
    public void setUp() {
        TestEnvironment.setUp();
    }

    @Test
    public void testList() throws JuspayException {
        testCreate();
        WalletList wallets = Wallet.list(customerTest.customer.getObjectReferenceId());
        assertNotNull(wallets.getList());
    }

    @Test
    public void testRefresh() throws JuspayException {
        testCreate();
        WalletList wallets = Wallet.refresh(customerTest.customer.getObjectReferenceId());
        assertNotNull(wallets.getList());
    }

    @Test
    public void testRefreshByWalletId() throws JuspayException {
        testCreate();
        wallet = Wallet.refreshByWalletId(wallet.getId());
        assertNotNull(wallet);
    }

    @Test
    public void testCreate() throws JuspayException {
        if (customerTest==null) {
            customerTest = new CustomerTest();
            customerTest.testCreate();
        }
        wallet = Wallet.create(customerTest.customer.getObjectReferenceId(), "MOBIKWIK");
        assertNotNull(wallet);
    }

    @Test
    public void testCreateWithMetadata() throws JuspayException {
        if (customerTest==null) {
            customerTest = new CustomerTest();
            customerTest.testCreate();
        }
        RequestOptions requestOptions = RequestOptions.createDefault().withApiVersion("2019-09-27");
        wallet = Wallet.create(customerTest.customer.getObjectReferenceId(), "MOBIKWIK", null, requestOptions);
        assertNotNull(wallet);
        assertNotNull(wallet.getMetadata());
    }

    @Test
    public void testCreateAndAuthenticate() throws JuspayException {
        if (customerTest==null) {
            customerTest = new CustomerTest();
            customerTest.testCreate();
        }
        wallet = Wallet.createAndAuthenticate(customerTest.customer.getObjectReferenceId(), "MOBIKWIK");
        assertNotNull(wallet);
    }

    @Test
    public void testAuthenticate() throws JuspayException {
        testCreate();
        wallet = Wallet.authenticate(wallet.getId());
        assertNotNull(wallet);
    }

}
