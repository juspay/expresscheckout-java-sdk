package in.juspay.test;

import in.juspay.exception.JuspayException;
import in.juspay.model.PaymentMethod;
import in.juspay.model.PaymentMethodList;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class PaymentMethodTest {

    @Before
    public void setUp() {
        TestEnvironment.setUp();
    }

    @Test
    public void testList() throws JuspayException {
        PaymentMethodList paymentMethodList = PaymentMethod.list();
        assertNotNull(paymentMethodList);
    }
}



