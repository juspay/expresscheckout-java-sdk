package in.juspay.test;

import in.juspay.model.JuspayEnvironment;

public class TestEnvironment {

    public static void setUp() {
        JuspayEnvironment.withBaseUrl(JuspayEnvironment.SANDBOX_BASE_URL)
                .withApiKey("").withMerchantId("");
    }
}